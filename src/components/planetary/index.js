import * as React from 'react'
import useNasaApi from '../../api/index'

function Planetary() {
  const { data } = useNasaApi()

  return (
    <figure
      className="w-4xl mb-16 md:mr-16 md:mb-0"
    >
      <img src={data.url} alt={data.title} loading="lazy" />

      <figcaption
        className="flex justify-center text-lg text-white bg-black"
      >
        {data.title}
      </figcaption>
    </figure>
  )
}

export default Planetary
