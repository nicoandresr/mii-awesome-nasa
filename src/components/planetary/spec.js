import * as React from 'react'
import { renderToStaticMarkup as render } from 'react-dom/server'

import Planetary from './'

it('Planetary', () => {
  expect(render(<Planetary />)).toMatchSnapshot()
})
