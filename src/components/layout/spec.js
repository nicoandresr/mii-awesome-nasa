import * as React from 'react'
import { renderToStaticMarkup as render } from 'react-dom/server'

import Layout from './'

it('Layout', () => {
  expect(render(<Layout>Hello world</Layout>)).toMatchSnapshot()
})
