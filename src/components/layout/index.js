import * as React from 'react'
import PropTypes from 'prop-types'

const propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
}

const defaultProps = {
}

function Layout({ children }) {
  return (
    <>
      <nav
        className="h-16 flex justify-center items-center bg-gray-800"
      >
        <h1
          className="text-xl"
        >
          Mii Awesome Nasa
        </h1>
      </nav>

      <main
        className="flex flex-grow flex-col md:flex-row items-center justify-start m-auto container"
      >
        {children}
      </main>

      <footer
        className="h-16 flex justify-center items-center bg-gray-500 text-gray-900 capitalize text-xl"
      >
        powered by&nbsp;
        <a href="gitlab.com/nicoandresr/mii-awesome-nasa" target="_blank">
          NicoandresR
        </a>
      </footer>
    </>
  )
}

Layout.propTypes = propTypes
Layout.defaultProps = defaultProps

export default Layout
