jest.mock('../../api/movie-db')

import * as React from 'react'
import { renderToStaticMarkup as render } from 'react-dom/server'
import useMovieDBApi from '../../api/movie-db'
import TitleList from './index'

const mockApiResult = {
  config: {
    images: { base_url: 'http://'}
  },
  titles: {
    results: [{
      id: '1',
      title: 'ab',
      poster_path: 'cd.e'
    }]
  }
}

test('title list', () => {
  useMovieDBApi.mockReturnValue(mockApiResult)

  expect(render(<TitleList />)).toMatchSnapshot()
})
