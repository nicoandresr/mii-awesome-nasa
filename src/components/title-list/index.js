import * as React from 'react'
import useMovieDBApi from '../../api/movie-db'
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css"
import "swiper/components/navigation/navigation.min.css"

import SwiperCore, {
  Autoplay,
  Pagination,
  Navigation,
  EffectCoverflow
} from 'swiper/core';

SwiperCore.use([Autoplay,Pagination,Navigation,EffectCoverflow]);

function TileList() {
  const { titles, config } = useMovieDBApi()
  const [current, setCurrent] = React.useState()
  const swiperEl = React.useRef(null)

  const onSwiperClick = event => {
    const { autoplay, activeIndex } = event

    if (autoplay.paused) {
      autoplay.run()
      return
    }

    setCurrent(titles.results[activeIndex])
    autoplay.pause()
  }

  const onCloseHandler = () => {
    setCurrent(null)
    swiperEl.current.swiper.autoplay.run()
  }

  return (
    <>
      <Swiper
        ref={swiperEl}
        effect='coverflow'
        onClick={onSwiperClick}
        centeredSlides
        slidesPerView="auto"
        className="max-w-full md:max-w-2xl mb-16 md:mb-0"
        autoplay={{ delay: 2500 }}
      >
        {titles.results.map(title => (
          <SwiperSlide
            key={title.id}
            style={{ width: '342px' }}
          >
            {config.images.base_url && (
              <figure>
                <img
                  className="rounded-xl"
                  src={`${config.images.base_url}w342/${title.poster_path}`}
                  loading="lazy"
                  alt={title.title}
                />
              </figure>
            )}
          </SwiperSlide>
        ))}
      </Swiper>


      {current && (
        <div
          className="bg-opacity-50 bg-black flex right-0 z-10 w-full h-full static md:absolute"
        >
          <section
            className="fixed md:static bottom-0 md:inset-0 text-gray-700 bg-gray-200 max-w-4xl rounded-xl mt-2 md:m-auto flex flex-col-reverse md:flex-row items-start justify-end md:justify-between"
          >
            <figure
              className="p-8 hidden md:block"
            >
              <img
                className="rounded-xl shadow-xl"
                src={`${config.images.base_url}w342/${current.poster_path}`}
                loading="lazy"
                alt={current.title}
              />
            </figure>

            <div className="flex flex-col h-full md:h-64 m-0 md:m-auto justify-start md:justify-center max-w-lg px-8 md:px-0">
              <h2
                className="text-3xl text-center font-bold"
              >
                {current.title}
              </h2>
              <p
                className="py-8 text-xl leading-6"
              >
                {current.overview}
              </p>
            </div>

            <button
              type="button"
              className="text-2xl p-8 uppercase font-bold mr-0 ml-auto md:m-0"
              onClick={onCloseHandler}
            >
              x
            </button>
          </section>
        </div>
      )}
    </>
  )
}

export default TileList
