import * as React from 'react'
import { renderToStaticMarkup as render } from 'react-dom/server'
import App from './app'

it('App match with snapshot', () => {
  expect(render(<App />)).toMatchSnapshot()
})
