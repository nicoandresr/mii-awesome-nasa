import * as React from 'react'

import Layout from './components/layout/index'
import TileList from './components/title-list/index'
import Planetary from './components/planetary/index'

function App() {
  return (
    <Layout>
      <Planetary />

      <TileList />
    </Layout>
  )
}

export default App
