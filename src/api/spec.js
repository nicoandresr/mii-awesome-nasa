import { normalize } from './'

it('Normalize data', () => {
  // Arrange
  const data = { data: '1' }
  // Act
  const result = normalize(data)
  // Assert
  expect(result).toEqual(data)
})
