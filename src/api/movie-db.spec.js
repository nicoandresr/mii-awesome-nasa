import { normalize } from './movie-db'

test('normalize titles', () => {
  // Arrange
  const data = { results: [{ poster_path: '1' }, { poster_path: null }] }
  const filtered = { results: [{ poster_path: '1' }] }
  // Act
  const result = normalize.titles(data)
  // Assert
  expect(result).toEqual(filtered)
})

test('normalize config', () => {
  // Arrange
  const data = { images: { base_url: '' } }
  // Act
  const result = normalize.config(data)
  // Assert
  expect(result).toEqual(data)
})
