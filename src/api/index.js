import useFetch from 'js-usefetch'
import { NASA_API_KEY } from '../secrets'

const url = `https://api.nasa.gov/planetary/apod?api_key=${NASA_API_KEY}`
const defaults = { url: '', title: 'loading...' }

export function normalize (data) {
  return data
}

function useNasaApi() {
  const { data } = useFetch({ url, defaults, normalize })
  return { data }
}

export default useNasaApi
