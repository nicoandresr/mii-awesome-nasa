import useFetch from 'js-usefetch'
import { MOVIE_DB_API_KEY } from '../secrets'

const host = `https://api.themoviedb.org/3`
const titlesEndPoint = `${host}/search/movie?api_key=${MOVIE_DB_API_KEY}&query=NASA&include_adult=false`
const configEndPoint = `${host}/configuration?api_key=${MOVIE_DB_API_KEY}`
const defaults = {
  titles: { results: [] },
  config: { images: { base_url: '' }}
}

export const normalize = {
  titles(data) {
    return {
      ...data,
      results: data.results.filter(title => !!title.poster_path)
    }
  },
  config(data) {
    return data
  }
}

function useMovieDBApi() {
  const { data: titles } = useFetch({
    url: titlesEndPoint,
    defaults: defaults.titles,
    normalize: normalize.titles
  })
  const { data: config } = useFetch({
    url: configEndPoint,
    defaults: defaults.config,
    normalize: normalize.config
  })

  return { titles, config }
}

export default useMovieDBApi
